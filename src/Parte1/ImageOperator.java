package Parte1;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


public class ImageOperator {
	 final JFileChooser selector = new JFileChooser ();
	 File imagen_seleccionada;	
	 BufferedImage imagen_original;
	 BufferedImage segunda_imagen;
	 BufferedImage imagen_trabajada;

	 int R[][], R1[][];
	 int G[][], G1[][];
	 int B[][], B1[][];
	 int TRANSFORMADA[][];
	 
	 int ancho;
	 int altura;
	
	public void AbrirImagen1() throws IOException
	{
		selector.setDialogTitle("Cargar Imagen 1");
		FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG & PNG", "jpg", "png");
		selector.setFileFilter(filtroImagen);
		int flag = selector.showOpenDialog(null);
		
		if(flag == JFileChooser.APPROVE_OPTION)
		{
				imagen_seleccionada = selector.getSelectedFile();
				imagen_original = ImageIO.read(imagen_seleccionada);
				ancho = imagen_original.getWidth();
				altura = imagen_original.getHeight();
				RGB1();
		}	
	}
	
	public void AbrirImagen2() throws IOException
	{
		selector.setDialogTitle("Cargar Imagen 2");
		FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG & PNG", "jpg", "png");
		selector.setFileFilter(filtroImagen);
		int flag = selector.showOpenDialog(null);
		
		if(flag == JFileChooser.APPROVE_OPTION)
		{
				imagen_seleccionada = selector.getSelectedFile();
				segunda_imagen= ImageIO.read(imagen_seleccionada);
				RGB2();
		}	
	}
	
	public void RGB1()
	{			
		
			Color color_aux;
			R = new int [ancho][altura];
			G = new int [ancho][altura];
			B = new int [ancho][altura];
			
			for(int i= 0; i<ancho; i++)
			{
				for(int j= 0; j<altura; j++)
				{

					color_aux = new Color(imagen_original.getRGB(i, j));
					R [i][j] = color_aux.getRed();
					G [i][j] = color_aux.getGreen();
					B [i][j] = color_aux.getBlue();
				}
			}
	}
	
	public void RGB2()
	{			
			Color color_aux;
			R1 = new int [ancho][altura];
			G1 = new int [ancho][altura];
			B1 = new int [ancho][altura];
			
			for(int i= 0; i<ancho; i++)
			{
				for(int j= 0; j<altura; j++)
				{

					color_aux = new Color(segunda_imagen.getRGB(i, j));
					R1 [i][j] = color_aux.getRed();
					G1 [i][j] = color_aux.getGreen();
					B1 [i][j] = color_aux.getBlue();
				}
			}
	}
	
	public void SumaImagen()
	{
		TRANSFORMADA = new int [ancho][altura];

		for(int i= 0; i<ancho; i++)
		{
			for(int j= 0; j<altura; j++)
			{
				TRANSFORMADA[i][j] = (int) ((R[i][j] + R1[i][j]));
				TRANSFORMADA[i][j] = (int) ((G[i][j] + G1[i][j]));
				TRANSFORMADA[i][j] = (int) ((B[i][j] + B1[i][j]));
			}
		}
	}
	
	public void RestaImagen()
	{
		TRANSFORMADA = new int [ancho][altura];

		for(int i= 0; i<ancho; i++)
		{
			for(int j= 0; j<altura; j++)
			{
				TRANSFORMADA[i][j] = (int) ((255/2) + (R[i][j] - R1[i][j])/2);
				TRANSFORMADA[i][j] = (int) ((255/2) + (G[i][j] - G1[i][j])/2);
				TRANSFORMADA[i][j] = (int) ((255/2) + (B[i][j] - B1[i][j])/2);
			}
		}
	}

	public void MultiplicarImagen()
	{
		TRANSFORMADA = new int [ancho][altura];

		for(int i= 0; i<ancho; i++)
		{
			for(int j= 0; j<altura; j++)
			{
				TRANSFORMADA[i][j] = (int) (R[i][j]*2 +  R1[i][j]*2);
				TRANSFORMADA[i][j] = (int) (G[i][j]*2 + G1[i][j]*2);
				TRANSFORMADA[i][j] = (int) (B[i][j]*2+ B1[i][j]*2);
			}
		}
	}
	
	public void CombinacionLineal()
	{
		TRANSFORMADA = new int [ancho][altura];

		for(int i= 0; i<ancho; i++)
		{
			for(int j= 0; j<altura; j++)
			{
				TRANSFORMADA[i][j] = (int) (R[i][j]*.5 +  R1[i][j]*.5);
				TRANSFORMADA[i][j] = (int) (G[i][j]*.5 +  G1[i][j]*.5);
				TRANSFORMADA[i][j] = (int) (B[i][j]*.5 +  B1[i][j]*.5);
			}
		}
	}
	
	public void CreaImagen(int [][] Imagen, int x, int y)
	{
		
		try
		{
			imagen_trabajada = new BufferedImage(x, y, imagen_original.getType());
			for(int i = 0; i<Imagen.length; i++)
			{
				for(int j = 0; j<Imagen[0].length; j++)
				{
					int rgb  = Imagen[i][j] << 16 | Imagen[i][j] <<8 | Imagen[i][j];
					imagen_trabajada.setRGB(i, j, new Color(rgb).getRGB());
				}			
			}
		}catch(Exception e) {System.out.println(e);}
	}
	
	
	public BufferedImage getImagenOriginal()
	{
		return imagen_original;
	}
	
	public BufferedImage getSegundaImagen()
	{
		return segunda_imagen;
	}
	
	public BufferedImage getImagenTrabajada()
	{
		return imagen_trabajada;
	}
	
	public int[][] getTransnformada()
	{
		return TRANSFORMADA;
	}
	
	public int getAncho()
	{
		return ancho;
	}
	
	public int getAltura()
	{
		return altura;
	}
}