package Parte1;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Image;
import java.awt.Panel;

public class GUI_ImageOperator extends JFrame {
	File archivo;
	JFileChooser seleccionado = new JFileChooser();	
	ImageOperator imgo = new ImageOperator();
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_ImageOperator frame = new GUI_ImageOperator();
					frame.setVisible(true);	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_ImageOperator() {		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 702, 443);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel Label1 = new JLabel("Imagen 1");
		Label1.setBounds(0, 11, 220, 300);
		contentPane.add(Label1);
		
		JLabel Label2 = new JLabel("Imagen 2");
		Label2.setBounds(225, 11, 220, 300);
		contentPane.add(Label2);
		
		JLabel Label3 = new JLabel("Imagen Transformada");
		Label3.setBounds(450, 11, 220, 300);
		contentPane.add(Label3);
		
		JButton btnCargarImagen1 = new JButton("Cargar Imagen 1");
		btnCargarImagen1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
						try {
							imgo.AbrirImagen1();
							Label1.setIcon(new ImageIcon(imgo.getImagenOriginal()));
							} catch (IOException e) {e.printStackTrace();}
				}
			}
		);
		btnCargarImagen1.setBounds(10, 324, 131, 23);
		contentPane.add(btnCargarImagen1);
		
		JButton btnCargarImagen2 = new JButton("Cargar Imagen 2");
		btnCargarImagen2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					imgo.AbrirImagen2();
					Label2.setIcon(new ImageIcon(imgo.getSegundaImagen()));
				} catch (IOException e) {e.printStackTrace();}
				
			}
		});
		btnCargarImagen2.setBounds(159, 324, 131, 23);
		contentPane.add(btnCargarImagen2);
		
		JButton btnNewButton_Suma = new JButton("+");
		btnNewButton_Suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				imgo.SumaImagen();
				imgo.CreaImagen(imgo.getTransnformada(), imgo.getAncho(), imgo.getAltura());
				Label3.setIcon(new ImageIcon(imgo.getImagenTrabajada()));
			}
		});
		btnNewButton_Suma.setBounds(300, 322, 50, 40);
		contentPane.add(btnNewButton_Suma);
		
		JButton btnNewButton_Resta = new JButton("-");
		btnNewButton_Resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				imgo.RestaImagen();
				imgo.CreaImagen(imgo.getTransnformada(), imgo.getAncho(), imgo.getAltura());
				Label3.setIcon(new ImageIcon(imgo.getImagenTrabajada()));
			}
		});
		btnNewButton_Resta.setBounds(353, 322, 50, 40);
		contentPane.add(btnNewButton_Resta);
		
		JButton btnNewButton_Multiplicar = new JButton("X");
		btnNewButton_Multiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				imgo.MultiplicarImagen();
				imgo.CreaImagen(imgo.getTransnformada(), imgo.getAncho(), imgo.getAltura());
				Label3.setIcon(new ImageIcon(imgo.getImagenTrabajada()));
			}
		});
		btnNewButton_Multiplicar.setBounds(406, 322, 50, 40);
		contentPane.add(btnNewButton_Multiplicar);
		
		JButton btnNewButton = new JButton("Combinacion_Lineal");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				imgo.CombinacionLineal();
				imgo.CreaImagen(imgo.getTransnformada(), imgo.getAncho(), imgo.getAltura());
				Label3.setIcon(new ImageIcon(imgo.getImagenTrabajada()));
			}
		});
		btnNewButton.setBounds(460, 322, 179, 40);
		contentPane.add(btnNewButton);
		
	}
}
